<?php 
    require "../utils/autoloader.php";

    class Modelo{
        protected $IpDB;
        protected $UsuarioDB;
        protected $PasswordDB;
        protected $NombreDB;
        protected $conexion;

        public function __construct(){
            $this -> inicializarConexion();

            $this -> conexion = new mysqli(
                $this -> IpDB,
                $this -> UsuarioDB,
                $this -> PasswordDB,
                $this -> NombreDB
            );
        }

        protected function inicializarConexion(){
            $this -> IpDB = IP_DB;
            $this -> UsuarioDB = USUARIO_DB;
            $this -> PasswordDB = PASSWORD_DB;
            $this -> NombreDB = NOMBRE_DB;
        }

    }
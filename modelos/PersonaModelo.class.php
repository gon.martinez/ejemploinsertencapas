<?php 
    require "../utils/autoloader.php";

    class PersonaModelo extends Modelo {
        public $id;
        public $nombre;
        public $apellido;
        public $edad;
        public $email;

        public function Guardar(){
            if($this -> id){
                $sql = "UPDATE persona SET nombre = ?, apellido = ?, edad = ?, email = ? WHERE id = ?";
                $sentencia = $this -> conexion -> prepare($sql);
                $sentencia -> bind_param("ssisi",
                    $this -> nombre,
                    $this -> apellido,
                    $this -> edad,
                    $this -> email,
                    $this -> id);
                $sentencia -> execute(); 
            }
            else {
                $sql = "INSERT INTO persona(nombre,apellido,edad,email) VALUES(?,?,?,?);";
                $sentencia = $this -> conexion -> prepare($sql);
                $sentencia -> bind_param("ssis",
                    $this -> nombre,
                    $this -> apellido,
                    $this -> edad,
                    $this -> email);
                $sentencia -> execute();
            }

        }
        public function Eliminar(){
            $sql = "DELETE FROM persona WHERE id = ?";
            $sentencia = $this -> conexion -> prepare($sql);
            $sentencia -> bind_param("i",
                $this -> id
            );

            $sentencia -> execute();

        }
        public function ObtenerTodos(){
            $sql = "SELECT * FROM persona";
            $personas = array();
            foreach($this -> conexion -> query($sql) -> fetch_all(MYSQLI_ASSOC) as $fila){
                $p = new PersonaModelo();
                $p -> id = $fila['id'];
                $p -> nombre = $fila['nombre'];
                $p -> apellido = $fila['apellido'];
                $p -> edad = $fila['edad'];
                $p -> email = $fila['email'];
                array_push($personas,$p);
            }

            return $personas;
            
        }

        public function ObtenerUno($id){
            $sql = "SELECT id,nombre,apellido,edad,email FROM persona WHERE id = ?";
            $sentencia = $this -> conexion -> prepare($sql);
            $sentencia -> bind_param("i",
                $this -> id
            );
            $sentencia -> execute();
            $fila = $sentencia -> get_result() -> fetch_assoc();
            var_dump($fila);
            $this -> id = $fila['id'];
            $this -> nombre = $fila['nombre'];
            $this -> apellido = $fila['apellido'];
            $this -> edad = $fila['edad'];
            $this -> email = $fila['email'];


        }
    }
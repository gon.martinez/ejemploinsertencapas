<?php 
    require "../utils/autoloader.php";

    class PersonaControlador{
        public static function AltaDePersona($nombre,$apellido,$edad,$email){
            $p = new PersonaModelo();
            $p -> nombre = $nombre;
            $p -> apellido = $apellido;
            $p -> edad = $edad;
            $p -> email = $email;
            $p -> Guardar();
            
            return true;
        }

        public static function EliminarPersona($id){
            $p = new PersonaModelo();
            $p -> ObtenerUno($id);
            $p -> Eliminar();
            

        }

        public static function ModificarPersona(){
            $p = new PersonaModelo();
            $p -> ObtenerUno(3);
            $p -> nombre = "Jose";
            $p -> Guardar();

        }
        public static function ObtenerPersonas(){
            $p = new PersonaModelo();
            $personas = array();
            foreach($p -> ObtenerTodos() as $fila){
                $persona = array(
                    "id" => $fila -> id,
                    "nombre" => $fila -> nombre,
                    "apellido" => $fila -> apellido,
                    "edad" => $fila -> edad,
                    "email" => $fila -> email
                );
                array_push($personas,$persona);
            }
            return $personas;
        }
    }